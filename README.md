# mvc-app-project


## Getting started


Hi and welcome, this repository contains the source code for a php-based web app that shows the
use of the MVC pattern.

Available online at https://php-mvc-app-project.vercel.app/

P.S: we are currently configuring and exposing the DB as remote for fixing the issue :)


Code is organized as follows:

1. A model file responsible for management all DB conections and queries.

2. A controller file in charge of coordinate the request from the view (index.php file) and model.

3. A view file, just the index.php file. The view file uses HTML as language markup and Java Script for
   management the app events on the view (clicks and modals)

4. A file supporting the activities for a user profile (teacher) --> teachers.php


5. Two folders for management style and images files, css and img respectively.


6. A folder that contains a sqldump file from a MySQL DB.


7. A folder storing screenshots from the app.



