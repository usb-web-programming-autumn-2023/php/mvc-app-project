-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost
-- Généré le : lun. 10 juil. 2023 à 01:52
-- Version du serveur : 10.3.38-MariaDB-0ubuntu0.20.04.1
-- Version de PHP : 7.4.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `velta_integrador`
--

-- --------------------------------------------------------

--
-- Structure de la table `assignments`
--

CREATE TABLE `assignments` (
  `id` int(11) NOT NULL,
  `Max` int(100) NOT NULL,
  `Assignment_name` varchar(255) NOT NULL,
  `class_id` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Déchargement des données de la table `assignments`
--

INSERT INTO `assignments` (`id`, `Max`, `Assignment_name`, `class_id`) VALUES
(57, 50, 'proyecto seguridad criterio 1', 2),
(58, 60, 'proyecto seguridad criterio 2', 2),
(59, 75, 'proyecto seguridad criterio 3', 2),
(60, 40, 'proyecto mascotas criterio 1', 3),
(61, 80, 'proyecto mascotas criterio 2', 3),
(62, 100, 'proyecto realidad v criterio 1', 4),
(63, 100, 'proyecto realidad v criterio 2', 4),
(64, 150, 'proyecto realidad v criterio 3', 4),
(65, 150, 'proyecto inmobiliario criterio 1', 5),
(66, 50, 'proyecto inmobiliario criterio 2', 5),
(67, 15, 'CS showcase', 2),
(68, 65, 'puntualidad y vestimenta', 2);

-- --------------------------------------------------------

--
-- Structure de la table `classes`
--

CREATE TABLE `classes` (
  `id` int(11) NOT NULL,
  `class_name` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Déchargement des données de la table `classes`
--

INSERT INTO `classes` (`id`, `class_name`) VALUES
(2, 'Proyecto seguridad'),
(3, 'Proyecto Mascotas'),
(4, 'Proyeco R. Virtual'),
(5, 'Proyecto Inmobiliario');

-- --------------------------------------------------------

--
-- Structure de la table `grades`
--

CREATE TABLE `grades` (
  `id` int(10) NOT NULL,
  `user_id` int(10) NOT NULL,
  `assignment_id` int(10) NOT NULL,
  `grade` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Déchargement des données de la table `grades`
--

INSERT INTO `grades` (`id`, `user_id`, `assignment_id`, `grade`) VALUES
(63, 38, 57, 12),
(64, 38, 58, 30),
(65, 38, 59, 46),
(66, 39, 57, 22),
(67, 39, 58, 45),
(68, 40, 57, 45),
(69, 40, 58, 45),
(70, 40, 59, 59),
(71, 41, 60, 15),
(72, 41, 61, 22),
(73, 42, 60, 34),
(75, 42, 61, 38),
(76, 43, 62, 80),
(77, 43, 63, 78),
(78, 43, 64, 98),
(79, 44, 62, 34),
(80, 44, 63, 55),
(81, 45, 65, 55),
(82, 45, 66, 40),
(83, 46, 65, 30),
(84, 46, 66, 50),
(85, 47, 65, 53),
(86, 38, 57, 12),
(87, 41, 61, 12),
(88, 38, 68, 23);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `Id` int(11) NOT NULL,
  `Username` varchar(32) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Full_name` varchar(64) NOT NULL,
  `class_id` int(10) NOT NULL,
  `user_type` int(2) NOT NULL,
  `Date` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`Id`, `Username`, `Password`, `Full_name`, `class_id`, `user_type`, `Date`) VALUES
(38, 'Proyecto Seguridad Student 1', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'Seguridad Student 1', 2, 0, 20190405),
(39, 'Proyecto Seguridad Student 2', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'Seguridad Student 2', 2, 0, 20190405),
(40, 'Proyecto Seguridad Student 3', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'Seguridad Student 3', 2, 0, 20190405),
(41, 'Proyecto Mascotas Student 1', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'Mascotas Student 1', 3, 0, 20190405),
(42, 'Proyecto Mascotas Student 2', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'Mascotas Student 2', 3, 0, 20190405),
(43, 'Proyecto R Virtual Student 1', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'R Virtual Student 1', 4, 0, 20190405),
(44, 'Proyecto R Virtual Student 2', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'R Virtual Student 2', 4, 0, 20190405),
(45, 'Proyecto Inmobiliario Student 1', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', ' Inmobiliario Student 1', 5, 0, 20190405),
(46, 'Proyecto Inmobiliario Student 2', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'Inmobiliario Student 2', 5, 0, 20190405),
(47, 'Proyecto Inmobiliario Student 3', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'Inmobiliario Student 3', 5, 0, 20190405),
(49, 'Teacher', 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', 'Teacher', 0, 1, 20190405),
(51, 'mary', 'usbbog23', 'Mary Rubiano', 2, 1, 20230710),
(52, 'ramiro', 'camila23', 'Ramiro Poveda', 2, 0, 20230710);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `assignments`
--
ALTER TABLE `assignments`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `classes`
--
ALTER TABLE `classes`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`Id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `assignments`
--
ALTER TABLE `assignments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT pour la table `classes`
--
ALTER TABLE `classes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
